"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ComInvoiceType;
(function (ComInvoiceType) {
    ComInvoiceType[ComInvoiceType["invoice"] = 1] = "invoice";
    ComInvoiceType[ComInvoiceType["invoices"] = 2] = "invoices";
})(ComInvoiceType = exports.ComInvoiceType || (exports.ComInvoiceType = {}));
