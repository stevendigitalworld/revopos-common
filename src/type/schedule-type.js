"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ScheduleType;
(function (ScheduleType) {
    ScheduleType[ScheduleType["Daily"] = 0] = "Daily";
    ScheduleType[ScheduleType["Weely"] = 1] = "Weely";
    ScheduleType[ScheduleType["Monthly"] = 2] = "Monthly";
})(ScheduleType = exports.ScheduleType || (exports.ScheduleType = {}));
