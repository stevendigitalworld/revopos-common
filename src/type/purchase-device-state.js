"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PurchaseDeviceState;
(function (PurchaseDeviceState) {
    PurchaseDeviceState[PurchaseDeviceState["AutoDetectError"] = 1] = "AutoDetectError";
    PurchaseDeviceState[PurchaseDeviceState["ConnectToServer"] = 2] = "ConnectToServer";
    PurchaseDeviceState[PurchaseDeviceState["DisconnectToServer"] = 3] = "DisconnectToServer";
    PurchaseDeviceState[PurchaseDeviceState["Activated"] = 4] = "Activated";
})(PurchaseDeviceState = exports.PurchaseDeviceState || (exports.PurchaseDeviceState = {}));
