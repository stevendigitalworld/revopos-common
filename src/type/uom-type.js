"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UOMType;
(function (UOMType) {
    UOMType[UOMType["quantity"] = 1] = "quantity";
    UOMType[UOMType["weight"] = 2] = "weight";
})(UOMType = exports.UOMType || (exports.UOMType = {}));
