"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CalcType;
(function (CalcType) {
    CalcType[CalcType["FixedAmount"] = 1] = "FixedAmount";
    CalcType[CalcType["Percentage"] = 2] = "Percentage";
})(CalcType = exports.CalcType || (exports.CalcType = {}));
