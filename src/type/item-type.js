"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ItemType;
(function (ItemType) {
    ItemType[ItemType["ItemByQuantity"] = 1] = "ItemByQuantity";
    ItemType[ItemType["ItemByWeight"] = 2] = "ItemByWeight";
    ItemType[ItemType["Service"] = 3] = "Service";
    ItemType[ItemType["Package"] = 4] = "Package";
    ItemType[ItemType["Combo"] = 5] = "Combo";
    ItemType[ItemType["Attribute"] = 6] = "Attribute";
    ItemType[ItemType["Variant"] = 7] = "Variant";
})(ItemType = exports.ItemType || (exports.ItemType = {}));
