"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OperationType;
(function (OperationType) {
    OperationType[OperationType["Open"] = 1] = "Open";
    OperationType[OperationType["Close"] = 2] = "Close";
})(OperationType = exports.OperationType || (exports.OperationType = {}));
