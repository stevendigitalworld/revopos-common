"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NoteType;
(function (NoteType) {
    NoteType[NoteType["till"] = 1] = "till";
    NoteType[NoteType["invoice"] = 2] = "invoice";
})(NoteType = exports.NoteType || (exports.NoteType = {}));
