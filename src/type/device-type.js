"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DeviceType;
(function (DeviceType) {
    DeviceType[DeviceType["Unknown"] = 0] = "Unknown";
    DeviceType[DeviceType["Revopos"] = 1] = "Revopos";
    DeviceType[DeviceType["Emenu"] = 2] = "Emenu";
    DeviceType[DeviceType["HotPotEmenu"] = 3] = "HotPotEmenu";
    DeviceType[DeviceType["CustomerDisplay"] = 4] = "CustomerDisplay";
    DeviceType[DeviceType["Queue"] = 5] = "Queue";
    DeviceType[DeviceType["KitchenDisplay"] = 6] = "KitchenDisplay";
})(DeviceType = exports.DeviceType || (exports.DeviceType = {}));
