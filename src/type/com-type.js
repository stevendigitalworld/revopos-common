"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ComType;
(function (ComType) {
    ComType[ComType["internal"] = 1] = "internal";
    ComType[ComType["signalr"] = 2] = "signalr";
    ComType[ComType["invoices"] = 3] = "invoices";
    ComType[ComType["invoice"] = 4] = "invoice";
    ComType[ComType["current_user"] = 5] = "current_user";
    ComType[ComType["refresh"] = 6] = "refresh";
    ComType[ComType["lock"] = 7] = "lock";
    ComType[ComType["unlock"] = 8] = "unlock";
    ComType[ComType["unlock_screen"] = 9] = "unlock_screen";
    ComType[ComType["lock_screen"] = 10] = "lock_screen";
})(ComType = exports.ComType || (exports.ComType = {}));
