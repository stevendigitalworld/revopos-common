"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Post coupon 后返回的status code
 */
var CouponStatus;
(function (CouponStatus) {
    CouponStatus[CouponStatus["Error"] = 0] = "Error";
    CouponStatus[CouponStatus["Suceess"] = 1] = "Suceess";
    CouponStatus[CouponStatus["Failure"] = -1] = "Failure";
})(CouponStatus = exports.CouponStatus || (exports.CouponStatus = {}));
