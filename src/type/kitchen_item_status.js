"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KitchenItemStatus;
(function (KitchenItemStatus) {
    KitchenItemStatus[KitchenItemStatus["NEW_ORDER"] = 1] = "NEW_ORDER";
    KitchenItemStatus[KitchenItemStatus["IN_PREPARE"] = 2] = "IN_PREPARE";
    KitchenItemStatus[KitchenItemStatus["PREPARED"] = 3] = "PREPARED";
    KitchenItemStatus[KitchenItemStatus["COMPLETED"] = 4] = "COMPLETED";
})(KitchenItemStatus = exports.KitchenItemStatus || (exports.KitchenItemStatus = {}));
