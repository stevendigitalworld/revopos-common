"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PosType;
(function (PosType) {
    PosType[PosType["tableSvc"] = 1] = "tableSvc";
    PosType[PosType["basic"] = 2] = "basic";
    PosType[PosType["tableDeilvery"] = 3] = "tableDeilvery";
    PosType[PosType["salon"] = 4] = "salon";
})(PosType = exports.PosType || (exports.PosType = {}));
