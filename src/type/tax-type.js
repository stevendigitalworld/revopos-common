"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaxType;
(function (TaxType) {
    TaxType[TaxType["Inclusive"] = 1] = "Inclusive";
    TaxType[TaxType["Exclusive"] = 2] = "Exclusive";
})(TaxType = exports.TaxType || (exports.TaxType = {}));
