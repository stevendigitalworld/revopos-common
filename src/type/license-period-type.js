"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LicensePeriodType;
(function (LicensePeriodType) {
    LicensePeriodType[LicensePeriodType["Month"] = 1] = "Month";
    LicensePeriodType[LicensePeriodType["Year"] = 2] = "Year";
})(LicensePeriodType = exports.LicensePeriodType || (exports.LicensePeriodType = {}));
