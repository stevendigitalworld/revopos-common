"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TableStatus;
(function (TableStatus) {
    TableStatus[TableStatus["Closed"] = 1] = "Closed";
    TableStatus[TableStatus["Opened"] = 2] = "Opened";
    TableStatus[TableStatus["Reserved"] = 3] = "Reserved";
    TableStatus[TableStatus["Payment"] = 4] = "Payment";
    TableStatus[TableStatus["Onhold"] = 5] = "Onhold";
    TableStatus[TableStatus["MultipleStatus"] = 6] = "MultipleStatus";
    TableStatus[TableStatus["Locked"] = 7] = "Locked";
    TableStatus[TableStatus["OpenedWithAnotherReservationOnHold"] = 8] = "OpenedWithAnotherReservationOnHold";
})(TableStatus = exports.TableStatus || (exports.TableStatus = {}));
