"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TagType;
(function (TagType) {
    TagType["report"] = "report";
    TagType["template"] = "template";
    TagType["potslider"] = "potslider";
    TagType["cdsfull"] = "cdsfull";
    TagType["cdshalf"] = "cdshalf";
})(TagType = exports.TagType || (exports.TagType = {}));
