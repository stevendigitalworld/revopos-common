"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TillType;
(function (TillType) {
    TillType[TillType["initial_deposit"] = 1] = "initial_deposit";
    TillType[TillType["invoice"] = 2] = "invoice";
    TillType[TillType["pay_in"] = 3] = "pay_in";
    TillType[TillType["pay_out"] = 4] = "pay_out";
})(TillType = exports.TillType || (exports.TillType = {}));
