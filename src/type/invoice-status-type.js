"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceStatusType;
(function (InvoiceStatusType) {
    InvoiceStatusType[InvoiceStatusType["Open"] = 0] = "Open";
    InvoiceStatusType[InvoiceStatusType["Onhold"] = 1] = "Onhold";
    InvoiceStatusType[InvoiceStatusType["Payment"] = 2] = "Payment";
    InvoiceStatusType[InvoiceStatusType["Close"] = 3] = "Close";
    InvoiceStatusType[InvoiceStatusType["Void"] = 4] = "Void";
})(InvoiceStatusType = exports.InvoiceStatusType || (exports.InvoiceStatusType = {}));
