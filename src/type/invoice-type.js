"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceType;
(function (InvoiceType) {
    InvoiceType[InvoiceType["DineIn"] = 1] = "DineIn";
    InvoiceType[InvoiceType["TakeAway"] = 2] = "TakeAway";
    InvoiceType[InvoiceType["Delivery"] = 3] = "Delivery";
})(InvoiceType = exports.InvoiceType || (exports.InvoiceType = {}));
