"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApplyingType;
(function (ApplyingType) {
    ApplyingType[ApplyingType["All"] = 1] = "All";
    ApplyingType[ApplyingType["Inclusive"] = 2] = "Inclusive";
    ApplyingType[ApplyingType["Exclusive"] = 3] = "Exclusive";
})(ApplyingType = exports.ApplyingType || (exports.ApplyingType = {}));
