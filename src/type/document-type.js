"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DocumentType;
(function (DocumentType) {
    DocumentType[DocumentType["PDF"] = 1] = "PDF";
    DocumentType[DocumentType["Excel"] = 2] = "Excel";
    DocumentType[DocumentType["Html"] = 3] = "Html";
})(DocumentType = exports.DocumentType || (exports.DocumentType = {}));
