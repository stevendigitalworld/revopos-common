"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CouponAction;
(function (CouponAction) {
    CouponAction[CouponAction["Validate"] = 1] = "Validate";
    CouponAction[CouponAction["Generate"] = 2] = "Generate";
    CouponAction[CouponAction["Purchase"] = 3] = "Purchase";
})(CouponAction = exports.CouponAction || (exports.CouponAction = {}));
