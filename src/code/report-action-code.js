"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ReportActionCode;
(function (ReportActionCode) {
    ReportActionCode[ReportActionCode["Print"] = 0] = "Print";
    ReportActionCode[ReportActionCode["Generate"] = 1] = "Generate";
    ReportActionCode[ReportActionCode["Email"] = 2] = "Email";
})(ReportActionCode = exports.ReportActionCode || (exports.ReportActionCode = {}));
