"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ReverseActionCode;
(function (ReverseActionCode) {
    ReverseActionCode[ReverseActionCode["CreateReservation"] = 1] = "CreateReservation";
    ReverseActionCode[ReverseActionCode["StartReservation"] = 2] = "StartReservation";
    ReverseActionCode[ReverseActionCode["CancelReservation"] = 3] = "CancelReservation";
    ReverseActionCode[ReverseActionCode["OnHoldReservation"] = 4] = "OnHoldReservation";
    ReverseActionCode[ReverseActionCode["RevertOnHoldReservation"] = 5] = "RevertOnHoldReservation";
})(ReverseActionCode = exports.ReverseActionCode || (exports.ReverseActionCode = {}));
