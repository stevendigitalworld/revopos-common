"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BellRequestCode;
(function (BellRequestCode) {
    BellRequestCode[BellRequestCode["Service"] = 1] = "Service";
    BellRequestCode[BellRequestCode["Bill"] = 2] = "Bill";
})(BellRequestCode = exports.BellRequestCode || (exports.BellRequestCode = {}));
