"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KitchenInvoiceActionCode;
(function (KitchenInvoiceActionCode) {
    KitchenInvoiceActionCode[KitchenInvoiceActionCode["KITCHEN_ITEM_TO_NEW_ORDER"] = 5000] = "KITCHEN_ITEM_TO_NEW_ORDER";
    KitchenInvoiceActionCode[KitchenInvoiceActionCode["KITCHEN_ITEM_TO_PREPARED"] = 5001] = "KITCHEN_ITEM_TO_PREPARED";
    KitchenInvoiceActionCode[KitchenInvoiceActionCode["KITCHEN_ITEM_TO_COMPLETED"] = 5002] = "KITCHEN_ITEM_TO_COMPLETED";
})(KitchenInvoiceActionCode = exports.KitchenInvoiceActionCode || (exports.KitchenInvoiceActionCode = {}));
