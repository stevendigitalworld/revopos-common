"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorCode;
(function (ErrorCode) {
    ErrorCode[ErrorCode["INTERNET_NOT_AVAILABLE"] = 100] = "INTERNET_NOT_AVAILABLE";
    ErrorCode[ErrorCode["UNAUTHORIZED_ACCESS"] = 101] = "UNAUTHORIZED_ACCESS";
    // invoice error code part
    ErrorCode[ErrorCode["INVOICE_INCORRECT"] = 201] = "INVOICE_INCORRECT";
    ErrorCode[ErrorCode["INVOICE_OUTDATED"] = 202] = "INVOICE_OUTDATED";
    ErrorCode[ErrorCode["INVOICE_TRANSACTED"] = 203] = "INVOICE_TRANSACTED";
    ErrorCode[ErrorCode["INVOICE_INCOMPLETE"] = 204] = "INVOICE_INCOMPLETE";
    ErrorCode[ErrorCode["INVOICE_NOT_FOUND_IN_DB"] = 205] = "INVOICE_NOT_FOUND_IN_DB";
    ErrorCode[ErrorCode["INVOICE_TABLE_SWITCH_FAILED"] = 206] = "INVOICE_TABLE_SWITCH_FAILED";
    ErrorCode[ErrorCode["INVOICE_DUPLICATE_FAILED_TABLE_NOT_EMPTY"] = 207] = "INVOICE_DUPLICATE_FAILED_TABLE_NOT_EMPTY";
    ErrorCode[ErrorCode["INVOICE_VOID_FAILED_INSUFFICIENT_BALANCE"] = 211] = "INVOICE_VOID_FAILED_INSUFFICIENT_BALANCE";
    ErrorCode[ErrorCode["INVOICE_VOID_FAILED_INSUFFICIENT_POINT"] = 212] = "INVOICE_VOID_FAILED_INSUFFICIENT_POINT";
    //
    ErrorCode[ErrorCode["ACTION_INVALID"] = 301] = "ACTION_INVALID";
    ErrorCode[ErrorCode["REQUEST_INVALID"] = 401] = "REQUEST_INVALID";
    ErrorCode[ErrorCode["OTP_Request_Daily_Limit_Exceeded"] = 411] = "OTP_Request_Daily_Limit_Exceeded";
    ErrorCode[ErrorCode["OTP_Request_Too_Frequent"] = 412] = "OTP_Request_Too_Frequent";
    ErrorCode[ErrorCode["OTP_Invalid"] = 413] = "OTP_Invalid";
    ErrorCode[ErrorCode["OTP_Expired"] = 414] = "OTP_Expired";
    ErrorCode[ErrorCode["SMS_Does_Not_Have_Enough_Credit"] = 420] = "SMS_Does_Not_Have_Enough_Credit";
    ErrorCode[ErrorCode["SMS_NOT_FOUND_IN_DB"] = 421] = "SMS_NOT_FOUND_IN_DB";
    ErrorCode[ErrorCode["SMS_SENDING_FAILURE"] = 422] = "SMS_SENDING_FAILURE";
    // customer error code part
    ErrorCode[ErrorCode["Incorrect_Account_Or_Password"] = 500] = "Incorrect_Account_Or_Password";
    ErrorCode[ErrorCode["Account_Exists"] = 501] = "Account_Exists";
    ErrorCode[ErrorCode["Account_Does_Not_Exist"] = 502] = "Account_Does_Not_Exist";
    ErrorCode[ErrorCode["ACCOUNT_ADDRESS_EXIST"] = 503] = "ACCOUNT_ADDRESS_EXIST";
    ErrorCode[ErrorCode["ACCOUNT_ADDRESS_DOES_NOT_EXIST"] = 504] = "ACCOUNT_ADDRESS_DOES_NOT_EXIST";
    ErrorCode[ErrorCode["ACCOUNT_TXN_EXIST"] = 505] = "ACCOUNT_TXN_EXIST";
    ErrorCode[ErrorCode["ACCOUNT_TXN_DOES_NOT_EXIST"] = 506] = "ACCOUNT_TXN_DOES_NOT_EXIST";
    // device error code part
    ErrorCode[ErrorCode["DEVICE_UID_NOT_IN_REQ"] = 600] = "DEVICE_UID_NOT_IN_REQ";
    ErrorCode[ErrorCode["DEVICE_EXIST"] = 601] = "DEVICE_EXIST";
    ErrorCode[ErrorCode["DEVICE_NOT_EXIST"] = 602] = "DEVICE_NOT_EXIST";
    ErrorCode[ErrorCode["DEVICE_LICENSE_QUOTA_EXCEEDED"] = 603] = "DEVICE_LICENSE_QUOTA_EXCEEDED";
    ErrorCode[ErrorCode["DEVICE_TYPE_NOT_AVAILABLE"] = 604] = "DEVICE_TYPE_NOT_AVAILABLE";
    // payment error code part
    ErrorCode[ErrorCode["PAYMENT_METHOD_EXIST"] = 701] = "PAYMENT_METHOD_EXIST";
    ErrorCode[ErrorCode["PAYMENT_METHOD_NOT_EXIST"] = 702] = "PAYMENT_METHOD_NOT_EXIST";
    // Miscellaneous
    ErrorCode[ErrorCode["Unknown_Error"] = 900] = "Unknown_Error";
    ErrorCode[ErrorCode["QR_Outdated_Cache_Invoice"] = 901] = "QR_Outdated_Cache_Invoice";
    ErrorCode[ErrorCode["QR_CACHE_INVOICE_NOT_FOUND_IN_DB"] = 902] = "QR_CACHE_INVOICE_NOT_FOUND_IN_DB";
    // license error code part
    ErrorCode[ErrorCode["LICENSE_FORMAT_INVALID"] = 1000] = "LICENSE_FORMAT_INVALID";
    ErrorCode[ErrorCode["LICENSE_SIGNATURE_EMPTY"] = 1001] = "LICENSE_SIGNATURE_EMPTY";
    ErrorCode[ErrorCode["LICENSE_SIGNATURE_ILLEGAL"] = 1002] = "LICENSE_SIGNATURE_ILLEGAL";
    ErrorCode[ErrorCode["LICENSE_KEY_NOT_FOUND"] = 1003] = "LICENSE_KEY_NOT_FOUND";
    ErrorCode[ErrorCode["LICENSE_KEY_SUSPENDED"] = 1004] = "LICENSE_KEY_SUSPENDED";
    ErrorCode[ErrorCode["LICENSE_KEY_HARDWARE_INVALID"] = 1005] = "LICENSE_KEY_HARDWARE_INVALID";
    ErrorCode[ErrorCode["LICENSE_KEY_USED"] = 1006] = "LICENSE_KEY_USED";
    ErrorCode[ErrorCode["LICENSE_KEY_TRIAL_USED"] = 1007] = "LICENSE_KEY_TRIAL_USED";
    ErrorCode[ErrorCode["LICENSE_INFO_INCOMPLETE"] = 1008] = "LICENSE_INFO_INCOMPLETE";
    ErrorCode[ErrorCode["LICENSE_SERVER_EXCEPTION"] = 1009] = "LICENSE_SERVER_EXCEPTION";
    ErrorCode[ErrorCode["LICENSE_DEVICE_ID_INVALID"] = 1010] = "LICENSE_DEVICE_ID_INVALID";
    ErrorCode[ErrorCode["LICENSE_DEVICE_QUOTA_EXCEEDED"] = 1011] = "LICENSE_DEVICE_QUOTA_EXCEEDED";
    ErrorCode[ErrorCode["LICENSE_ACTIVATION_QUOTA_INVALID"] = 1012] = "LICENSE_ACTIVATION_QUOTA_INVALID";
    ErrorCode[ErrorCode["LICENSE_NOT_FOUND"] = 1013] = "LICENSE_NOT_FOUND";
    ErrorCode[ErrorCode["LICENSE_ACTIVATION_TOTP_INVALID"] = 1014] = "LICENSE_ACTIVATION_TOTP_INVALID";
    ErrorCode[ErrorCode["LICENSE_DEVICE_ID_NOT_REGISTERED"] = 1015] = "LICENSE_DEVICE_ID_NOT_REGISTERED";
    ErrorCode[ErrorCode["LICENSE_DEVICE_ID_SUSPICIOUS"] = 1016] = "LICENSE_DEVICE_ID_SUSPICIOUS";
    // user error code 
    ErrorCode[ErrorCode["USER_SELECTED_SHOP_INVALID"] = 1100] = "USER_SELECTED_SHOP_INVALID";
    ErrorCode[ErrorCode["USER_SUBSCRIPTION_DEVICE_REQUEST_INVALID"] = 1101] = "USER_SUBSCRIPTION_DEVICE_REQUEST_INVALID";
    ErrorCode[ErrorCode["USER_PAYMENT_SOURCE_NOT_FOUND"] = 1102] = "USER_PAYMENT_SOURCE_NOT_FOUND";
    ErrorCode[ErrorCode["USER_PURCHASE_LICENSE_REQUEST_INVALID"] = 1103] = "USER_PURCHASE_LICENSE_REQUEST_INVALID";
    ErrorCode[ErrorCode["USER_PURCHASE_DEVICE_REQUEST_INVALID"] = 1104] = "USER_PURCHASE_DEVICE_REQUEST_INVALID";
    ErrorCode[ErrorCode["USER_PURCHASE_EXPIRY_DATE_NOT_IN_A_WHOLE_MONTH"] = 1105] = "USER_PURCHASE_EXPIRY_DATE_NOT_IN_A_WHOLE_MONTH";
    ErrorCode[ErrorCode["USER_PURCHASE_PAYMENT_FAILED"] = 1106] = "USER_PURCHASE_PAYMENT_FAILED";
    ErrorCode[ErrorCode["USER_HAS_NO_OWNERSHIP"] = 1107] = "USER_HAS_NO_OWNERSHIP";
    ErrorCode[ErrorCode["USER_REGISTRATION_INFO_INCOMPLETE"] = 1108] = "USER_REGISTRATION_INFO_INCOMPLETE";
    ErrorCode[ErrorCode["USER_REGISTRATION_ACCOUNT_EXIST"] = 1109] = "USER_REGISTRATION_ACCOUNT_EXIST";
    ErrorCode[ErrorCode["USER_EMAIL_IS_CONFIRMED"] = 1110] = "USER_EMAIL_IS_CONFIRMED";
    ErrorCode[ErrorCode["USER_ACCOUNT_DOES_NOT_EXIST"] = 1111] = "USER_ACCOUNT_DOES_NOT_EXIST";
    ErrorCode[ErrorCode["USER_EMAIL_EXIST"] = 1112] = "USER_EMAIL_EXIST";
    ErrorCode[ErrorCode["USER_PHONE_NO_IS_INVALID"] = 1113] = "USER_PHONE_NO_IS_INVALID";
    ErrorCode[ErrorCode["USER_OTP_IS_INVALID"] = 1114] = "USER_OTP_IS_INVALID";
    ErrorCode[ErrorCode["USER_ADD_OR_UPDATE_PHONE_NO"] = 1116] = "USER_ADD_OR_UPDATE_PHONE_NO";
    ErrorCode[ErrorCode["AUTH_OTP_SMS_REQUEST_DAILY_LIMIT_EXCEEDED"] = 6000] = "AUTH_OTP_SMS_REQUEST_DAILY_LIMIT_EXCEEDED";
    ErrorCode[ErrorCode["AUTH_OTP_SMS_REQUEST_TOO_FREQUENT"] = 6001] = "AUTH_OTP_SMS_REQUEST_TOO_FREQUENT";
    ErrorCode[ErrorCode["AUTH_OTP_SMS_VERFIY_OTP_INCORRECT"] = 6002] = "AUTH_OTP_SMS_VERFIY_OTP_INCORRECT";
    ErrorCode[ErrorCode["AUTH_OTP_SMS_VERIFY_ACCOUNT_INVALID"] = 6003] = "AUTH_OTP_SMS_VERIFY_ACCOUNT_INVALID";
    ErrorCode[ErrorCode["AUTH_OTP_SMS_PHONE_NUMBER_INVALID"] = 6004] = "AUTH_OTP_SMS_PHONE_NUMBER_INVALID";
    ErrorCode[ErrorCode["AUTH_OTP_SMS_LICENSE_NOT_FOUND"] = 6005] = "AUTH_OTP_SMS_LICENSE_NOT_FOUND";
    ErrorCode[ErrorCode["AUTH_OTP_SMS_VERIFY_OTP_EXPIRED"] = 6006] = "AUTH_OTP_SMS_VERIFY_OTP_EXPIRED";
    // Custom error code
    // Device
    ErrorCode[ErrorCode["CANT_READ_DEVICE"] = 10601] = "CANT_READ_DEVICE";
    ErrorCode[ErrorCode["CANT_REGISTER_DEVICE"] = 10602] = "CANT_REGISTER_DEVICE";
    // Version
    ErrorCode[ErrorCode["SERVER_VERSION_MISMATCH"] = 10401] = "SERVER_VERSION_MISMATCH";
})(ErrorCode = exports.ErrorCode || (exports.ErrorCode = {}));
var ErrorType;
(function (ErrorType) {
    ErrorType[ErrorType["HttpError"] = 1] = "HttpError";
    ErrorType[ErrorType["ServerError"] = 2] = "ServerError";
    ErrorType[ErrorType["ResponseError"] = 3] = "ResponseError";
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
var ErrorModel = /** @class */ (function () {
    function ErrorModel(error_code, error_msg) {
        this.error_code = error_code;
        this.error_msg = error_msg;
    }
    return ErrorModel;
}());
exports.ErrorModel = ErrorModel;
