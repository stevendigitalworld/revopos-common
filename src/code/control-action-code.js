"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ControlActionCode;
(function (ControlActionCode) {
    ControlActionCode[ControlActionCode["Update"] = 1] = "Update";
    ControlActionCode[ControlActionCode["Register"] = 2] = "Register";
})(ControlActionCode = exports.ControlActionCode || (exports.ControlActionCode = {}));
