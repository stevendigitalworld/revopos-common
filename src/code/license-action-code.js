"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LicenseActionCode;
(function (LicenseActionCode) {
    LicenseActionCode[LicenseActionCode["LICENSE_ACTIVATE"] = 1000] = "LICENSE_ACTIVATE";
    LicenseActionCode[LicenseActionCode["LICENSE_ACTIVATE_OFFLINE_VERIFY"] = 1003] = "LICENSE_ACTIVATE_OFFLINE_VERIFY";
})(LicenseActionCode = exports.LicenseActionCode || (exports.LicenseActionCode = {}));
