"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceHistoryActionCode;
(function (InvoiceHistoryActionCode) {
    InvoiceHistoryActionCode[InvoiceHistoryActionCode["ReprintKitchenSlip"] = 1] = "ReprintKitchenSlip";
    InvoiceHistoryActionCode[InvoiceHistoryActionCode["ReprintOrderSlip"] = 2] = "ReprintOrderSlip";
    InvoiceHistoryActionCode[InvoiceHistoryActionCode["ReprintAll"] = 3] = "ReprintAll";
})(InvoiceHistoryActionCode = exports.InvoiceHistoryActionCode || (exports.InvoiceHistoryActionCode = {}));
