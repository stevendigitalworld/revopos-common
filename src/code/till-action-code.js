"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TillActionCode;
(function (TillActionCode) {
    TillActionCode[TillActionCode["open_drawer"] = 1] = "open_drawer";
    TillActionCode[TillActionCode["pay_in"] = 2] = "pay_in";
    TillActionCode[TillActionCode["pay_out"] = 3] = "pay_out";
})(TillActionCode = exports.TillActionCode || (exports.TillActionCode = {}));
