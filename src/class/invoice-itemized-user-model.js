"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceItemizedUserModel = /** @class */ (function () {
    function InvoiceItemizedUserModel(itemized, user) {
        this.itemized = itemized;
        this.user = user;
        this.id = 0;
        this.invoice_itemized_id = 0;
        this.user_id = 0;
        if (!itemized && !user) {
            this.id = 0;
            this.invoice_itemized_id = itemized.id;
            this.user_id = user.id;
        }
    }
    return InvoiceItemizedUserModel;
}());
exports.InvoiceItemizedUserModel = InvoiceItemizedUserModel;
