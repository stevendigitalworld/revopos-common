"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ModifierModel = /** @class */ (function () {
    function ModifierModel(is_delete, item_modifier_id, user_id) {
        this.group = 0;
        this.group_quota = 0;
        this.price = 0;
        this.is_print = true;
        this.is_delete = false;
        if (is_delete)
            this.is_delete = is_delete;
        if (item_modifier_id)
            this.item_modifier_id = item_modifier_id;
        if (user_id)
            this.user_id = user_id;
    }
    return ModifierModel;
}());
exports.ModifierModel = ModifierModel;
