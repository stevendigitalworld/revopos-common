"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Attribute = /** @class */ (function () {
    function Attribute() {
        this.attribute_item = [];
        this.error = null;
        this.id = 0;
        this.name = "";
        this.alias = "";
    }
    return Attribute;
}());
exports.Attribute = Attribute;
var AttributeItem = /** @class */ (function () {
    function AttributeItem() {
        this.id = 0;
        this.attribute_id = 0;
        this.name = "";
        this.alias = "";
    }
    return AttributeItem;
}());
exports.AttributeItem = AttributeItem;
var Variant = /** @class */ (function () {
    function Variant() {
        this.item = {
            name: "",
            alias: "",
            price: 0,
            cost: 0,
            stock: 0,
            codename: "",
            barcode: "",
            expand: false
        };
        this.variant_definition = [];
    }
    return Variant;
}());
exports.Variant = Variant;
