"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceDiscountModel = /** @class */ (function () {
    function InvoiceDiscountModel(invoice, coupon_purchase, discount_amount) {
        this.invoice_id = invoice.id;
        this.coupon_purchase_itemized_id = coupon_purchase.id;
        this.name = coupon_purchase.coupon.name;
        this.discount_calc_type = coupon_purchase.coupon.discount_calc_type;
        this.discount_percent = coupon_purchase.coupon.discount_percent;
        this.discount_amount = discount_amount;
    }
    return InvoiceDiscountModel;
}());
exports.InvoiceDiscountModel = InvoiceDiscountModel;
