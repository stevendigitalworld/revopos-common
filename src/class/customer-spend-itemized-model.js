"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CustomerSpendItemizedModel = /** @class */ (function () {
    function CustomerSpendItemizedModel(p, qty) {
        this.customer_purchase_id = p.customer_purchase_id;
        this.item_id = p.item_id;
        this.quantity = qty;
    }
    return CustomerSpendItemizedModel;
}());
exports.CustomerSpendItemizedModel = CustomerSpendItemizedModel;
