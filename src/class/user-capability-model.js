"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserCapabilityModel = /** @class */ (function () {
    function UserCapabilityModel(role) {
        this.role = role;
        if (role) {
            if (role == 1) {
                this.enable_open_drawer = true;
                this.display_till_balance = true;
                this.enable_modify_layout = true;
                this.management_modify = true;
                this.setting_modify = true;
                this.crm_modify = true;
            }
            else if (role == 2) {
                this.enable_open_drawer = true;
                this.display_till_balance = true;
                this.enable_modify_layout = true;
                this.management_modify = true;
                this.setting_modify = false;
                this.crm_modify = true;
            }
            else if (role == 3) {
                this.enable_open_drawer = false;
                this.display_till_balance = false;
                this.enable_modify_layout = false;
                this.management_modify = false;
                this.setting_modify = false;
                this.crm_modify = false;
            }
            else {
                this.Default();
            }
        }
        else {
            this.Default();
        }
    }
    UserCapabilityModel.prototype.Default = function () {
        this.enable_open_drawer = false;
        this.crm_modify = false;
    };
    return UserCapabilityModel;
}());
exports.UserCapabilityModel = UserCapabilityModel;
