import { IVoucherItemized } from "../interface/i.voucher.itemized";
export declare class VoucherItemizedModel implements IVoucherItemized {
    voucher_id?: number;
    quantity?: number;
    discount_amount?: number;
}
