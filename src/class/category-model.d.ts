import { ICategory } from './../interface/i.category';
export declare class CategoryModel implements ICategory {
    name?: string;
    description?: string;
}
