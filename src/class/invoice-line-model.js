"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var item_type_1 = require("../type/item-type");
var InvoiceLineModel = /** @class */ (function () {
    function InvoiceLineModel(item, quantity, itemized) {
        this.id = 0;
        this.state = 0;
        this.is_delete = false;
        this.is_take_out = false;
        this.remark = null;
        this.discount_percent = 0;
        this.discount_amount = 0;
        this.discount_calc_type = 2;
        this.subtotal_before_discount = 0;
        this.combo_actual_price = 0;
        this.item = item;
        this.uom_type = 1;
        this.item_id = item.id;
        this.quantity = 0;
        this.subtotal = +(((+quantity) * item.price).toFixed(2));
        this.invoice_itemized = [];
        this.invoice_itemized.push(itemized);
        if (item.discount_calc_type == 2 && item.discount_percent != 0 && item.discountable && item.type != item_type_1.ItemType.ItemByWeight) {
            this.discount_calc_type = 2;
            this.discount_percent = item.discount_percent;
        }
    }
    return InvoiceLineModel;
}());
exports.InvoiceLineModel = InvoiceLineModel;
