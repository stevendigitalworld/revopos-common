export declare class InvoiceItemizedModifierModel {
    itemized?: any;
    modifier?: any;
    id?: number;
    invoice_itemized_id?: number;
    item_modifier_id?: number;
    is_delete?: boolean;
    constructor(itemized?: any, modifier?: any);
}
