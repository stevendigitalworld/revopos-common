import { IUserCapability } from "../interface/i.user.capability";
export declare class UserCapabilityModel implements IUserCapability {
    role?: number | undefined;
    enable_open_drawer?: boolean;
    display_till_balance?: boolean;
    enable_modify_layout?: boolean;
    management_modify?: boolean;
    setting_modify?: boolean;
    crm_modify?: boolean;
    constructor(role?: number | undefined);
    private Default;
}
