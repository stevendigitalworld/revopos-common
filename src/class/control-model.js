"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ControlModel = /** @class */ (function () {
    function ControlModel(device_type, config_version) {
        this.type = device_type;
        this.version = config_version;
    }
    return ControlModel;
}());
exports.ControlModel = ControlModel;
