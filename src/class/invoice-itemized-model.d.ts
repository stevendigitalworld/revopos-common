import { InvoiceItemizedUserModel } from './invoice-itemized-user-model';
import { InvoiceItemizedModifierModel } from './invoice-itemized-modifier-model';
import { IInvoiceItemized } from '../interface/i.invoice.itemized';
export declare class InvoiceItemizedModel implements IInvoiceItemized {
    id?: number;
    invoice_itemized_modifier?: InvoiceItemizedModifierModel[];
    invoice_itemized_user?: InvoiceItemizedUserModel[];
    invoice_line_id?: number;
    actual_price?: number;
    actual_price_before_discount?: number;
    discount_calc_type: number;
    discount_amount: number;
    discount_percent: number;
    unit_price: number;
    remark: any;
    is_delete: number;
    state: number;
    customer_id: number;
    guid: any;
    is_take_out: boolean;
    constructor(item: any);
}
