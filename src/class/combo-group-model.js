"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ComboGroupModel = /** @class */ (function () {
    function ComboGroupModel() {
        this.combo_item = [];
        this.sequence = 0;
    }
    return ComboGroupModel;
}());
exports.ComboGroupModel = ComboGroupModel;
