export declare class CustomerSpendItemizedModel {
    customer_purchase_id: number;
    item_id: number;
    quantity: number;
    constructor(p: any, qty: number);
}
