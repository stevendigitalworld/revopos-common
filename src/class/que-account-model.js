"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var QueAccountModel = /** @class */ (function () {
    function QueAccountModel() {
        this.recipients = [];
        this.error = null;
    }
    return QueAccountModel;
}());
exports.QueAccountModel = QueAccountModel;
