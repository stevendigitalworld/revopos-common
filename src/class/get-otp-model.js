"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GetOTPModel = /** @class */ (function () {
    function GetOTPModel() {
        this.customer = {
            phone_no: '',
            phone_country_code: ''
        };
        this.shop_license_id = 110;
        this.request_code = 1;
    }
    return GetOTPModel;
}());
exports.GetOTPModel = GetOTPModel;
