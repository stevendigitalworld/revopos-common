export interface InvoicePaymentModel {
    id: number;
    payment_method: any;
    invoice_id: number;
    tender_amount: number;
    payment_method_id: number;
}
