"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ItemModel = /** @class */ (function () {
    function ItemModel() {
        this.unit = 1;
        this.modifier = [];
        this.user = [];
        this.is_modifier_group = false;
        this.is_tax_exempt = false;
        this.is_service_charge_exempt = false;
        this.discount_calc_type = 0;
        this.discount_amount = 0;
        this.discount_percent = 0;
    }
    return ItemModel;
}());
exports.ItemModel = ItemModel;
