"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceItemizedModel = /** @class */ (function () {
    function InvoiceItemizedModel(item) {
        this.discount_calc_type = 2;
        this.discount_amount = 0;
        this.discount_percent = 0;
        this.is_take_out = false;
        this.id = 0;
        this.invoice_itemized_modifier = [];
        this.invoice_itemized_user = [];
        this.invoice_line_id = 0;
        this.actual_price = item.price;
        this.unit_price = item.price;
        this.actual_price_before_discount = item.price;
        this.discount_calc_type = 2;
        this.discount_amount = 0;
        this.discount_percent = 0;
        this.remark = null;
        this.is_delete = 0;
        this.state = 0;
        this.customer_id = 0;
    }
    return InvoiceItemizedModel;
}());
exports.InvoiceItemizedModel = InvoiceItemizedModel;
