"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PaymentModel = /** @class */ (function () {
    function PaymentModel() {
        this.tender_amount_alt = 0;
        this.change_amount_alt = 0;
        this.is_delete = false;
    }
    return PaymentModel;
}());
exports.PaymentModel = PaymentModel;
