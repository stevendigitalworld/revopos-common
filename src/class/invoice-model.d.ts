import { IInvoicePayment } from './../interface/i.invoice-payment';
import { InvoiceSignatureModel } from './invoice-signature-model';
import { InvoiceLineModel } from './invoice-line-model';
import { IInvoice } from '../interface/i.invoice';
export declare class InvoiceModel implements IInvoice {
    id?: number;
    device_id?: number;
    user_id?: number;
    invoice_signature?: InvoiceSignatureModel[];
    payment?: IInvoicePayment[];
    table_switch_id?: number;
    ref_id?: any;
    table_id?: number;
    type?: number;
    pax?: number;
    adult_pax?: number;
    child_pax?: number;
    baby_pax?: number;
    subtotal?: number;
    service_charge?: number;
    discount?: number;
    discount_type?: number;
    discount_amount?: number;
    tax?: number;
    tip?: number;
    rounding?: number;
    grand_total?: number;
    customer_id?: number;
    voucher_id?: number;
    status?: number;
    datetime_open?: Date;
    datetime_close?: Date;
    invoice_line?: InvoiceLineModel[];
    action?: number;
    deposit?: number;
    device_uuid?: any;
    kitchen_print_override?: boolean;
	etc?:Etc;
    constructor(table?: any, uuid?: any);
}

export class Etc implements IEtc {
    /** Reasons for voiding the invoice */
    void_reasons?: string | undefined;
    /** Date and time the invoice is being voided */
    voided_at?: Date ;
    /** The user id who voided the invoice */
    voided_by?: Number;
    /** If true, another invoice is cloned from this invoice */
    is_parent?: boolean;
    /** if is_parent is true,this invoice is a parent invoice from which a child invoice is cloned,
then there will be a child invoice id */
    child_invoice_id?: number;
    /** If true, another invoice is cloned from this invoice */
    is_child?: boolean;
    /** if is_child is true, this invoice is child invoice cloned from a parent invoice
then there will be a parent invoice id */
    parent_invoice_id?: number;
}

/** Extra information about invoice */
export interface IEtc {
    /** Reasons for voiding the invoice */
    void_reasons?: string | undefined;
    /** Date and time the invoice is being voided */
    voided_at?: Date | undefined;
    /** The user id who voided the invoice */
    voided_by?: Number | undefined;
    /** If true, another invoice is cloned from this invoice */
    is_parent?: boolean | undefined;
    /** if is_parent is true,this invoice is a parent invoice from which a child invoice is cloned,
then there will be a child invoice id */
    child_invoice_id?: number | undefined;
    /** If true, another invoice is cloned from this invoice */
    is_child?: boolean | undefined;
    /** if is_child is true, this invoice is child invoice cloned from a parent invoice
then there will be a parent invoice id */
    parent_invoice_id?: number | undefined;
}