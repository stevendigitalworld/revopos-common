import { IPackageItem } from "../interface/i.package.item";
export declare class PackageItem implements IPackageItem {
    item_id: number;
    package_id: number;
    quantity?: number;
    is_delete?: boolean;
}
