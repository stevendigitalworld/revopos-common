"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Combo = /** @class */ (function () {
    function Combo(itemId) {
        this.combo_group = [];
        if (itemId)
            this.item_id = itemId;
    }
    return Combo;
}());
exports.Combo = Combo;
