"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceModel = /** @class */ (function () {
    function InvoiceModel(table, uuid) {
        this.id = 0;
        this.type = 0;
        this.discount = 0;
        this.discount_type = 0;
        this.discount_amount = 0;
        this.deposit = 0;
        if (table)
            this.table_id = table.id, this.invoice_line = [], this.type = 1;
        if (uuid)
            this.device_uuid = uuid;
    }
    return InvoiceModel;
}());
exports.InvoiceModel = InvoiceModel;
