"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InvoiceItemizedModifierModel = /** @class */ (function () {
    function InvoiceItemizedModifierModel(itemized, modifier) {
        this.itemized = itemized;
        this.modifier = modifier;
        if (!itemized || !modifier) {
            this.id = 0;
            this.invoice_itemized_id = itemized.id;
            this.item_modifier_id = modifier.id;
            this.is_delete = false;
        }
    }
    return InvoiceItemizedModifierModel;
}());
exports.InvoiceItemizedModifierModel = InvoiceItemizedModifierModel;
