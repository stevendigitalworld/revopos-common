export interface IUserCapability {
    enable_open_drawer?: boolean;
    display_till_balance?: boolean;
    enable_modify_layout?: boolean;
    management_modify?: boolean;
    setting_modify?: boolean;
    crm_modify?: boolean;
    role?: number;
}
