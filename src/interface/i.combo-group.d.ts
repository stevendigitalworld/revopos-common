import { ComboItemModel } from './../class/combo-item-model';
export interface IComboGroup {
    combo_item: Array<ComboItemModel>;
    id?: number;
    combo_id?: number;
    name?: string;
    quota?: number;
    is_delete?: false;
    sequence: number;
}
