export interface ILicenseRequest {
    action?: number;
    license?: string;
    cpu_id?: string;
    ip?: string;
}
