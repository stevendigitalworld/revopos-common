export interface IPackageItem {
    item_id: number;
    package_id: number;
    quantity?: number;
    is_delete?: boolean;
}
