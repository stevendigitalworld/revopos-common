export interface IModifier {
    group: number;
    group_quota: number;
    price: number;
    name?: string;
    is_print: boolean;
    item_id?: number;
    is_delete?: boolean;
    item_modifier_id?: number;
    user_id?: any;
}
