import { IBaseItem } from './i.item';
export interface IComboItem {
    item?: IBaseItem;
    id?: number;
    item_id?: number;
    combo_group_id?: number;
    price?: number;
    is_delete?: false;
}
