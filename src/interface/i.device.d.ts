export interface IDevice {
    api_key?: any;
    error?: any;
    id?: number;
    uid?: string;
    type?: number;
    version?: string;
    platform?: string;
    platform_version?: string;
    device_model?: string;
    datetime_create?: string;
    is_delete?: boolean;
}
