import { IItem } from './i.item';
export interface ICustomerPurchase {
    available_quantity?: number;
    item?: IItem;
    datetime?: Date;
    id?: number;
    customer_purchase_id?: number;
    item_id?: number;
    quantity?: number;
    is_delete?: boolean;
}
