export interface ITill {
    id?: number;
    user_id?: number;
    operation_history_id?: number;
    invoice_id?: number;
    type?: number;
    status?: number;
    amount?: number;
    note?: string;
    datetime?: Date;
    is_delete?: boolean;
    action?: number;
}
