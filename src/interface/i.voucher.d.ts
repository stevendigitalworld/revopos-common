export interface IVoucher {
    id?: number;
    quantity?: number;
    discount_amount?: number;
    datetime_create?: string;
    is_delete?: boolean;
}
