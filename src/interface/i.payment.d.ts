export interface IPayment {
    id?: number;
    invoice_id?: number;
    tender_amount?: number;
    tender_amount_alt: number;
    change_amount?: number;
    change_amount_alt: number;
    payment_method_id?: number;
    note?: string;
    custom?: string;
    is_delete: boolean;
    payment_method?: string;
    rest?: number;
    is_cash?: boolean;
}
