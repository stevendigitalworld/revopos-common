# How to install?

```npm install git+ssh://git@bitbucket.org:vampuke/revopos-common.git```

### You can specific the version of revopos-common by adding version in the end of command.

```npm install git+ssh://git@bitbucket.org:vampuke/revopos-common.git#1.0.1```

# How to add new file?

### Clone revopos-common-repo, edit in this repo, run ```npm run build``` after edit, then commit the revopos-common repo to the origin with version as tag.

# Command about tag

## Add a tag 
### eg: version: 3.1.1
```git tag 3.1.1```

## Delete a tag 
```git tag -d 3.1.1```

## Push tag 
### Push all tags
```git push --tags```
### Push a specific tag
```git push origin 3.10.1```


# How to use?

### Add the following code to ts file when you want to import data from revopos-common.
### eg: TableModel
 ```import { TableModel } from 'revopos-common';```
### Then you could create model as usual.
```let table = new TableModel();``` 